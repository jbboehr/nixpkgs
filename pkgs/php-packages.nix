{ pkgs, fetchgit, php, handlebars, pkgconfig, talloc, libsodium, libossp_uuid, libuuid, autoreconfHook, imagemagick }:

let
  self = with self; {
    buildPecl = import <nixpkgs/pkgs/build-support/build-pecl.nix> {
      inherit php;
      inherit (pkgs) stdenv autoreconfHook fetchurl;
    };
  isPhpOlder55 = pkgs.lib.versionOlder php.version "5.5";
  isPhp7 = pkgs.lib.versionAtLeast php.version "7.0";

  apcu518 = buildPecl {
    name = "apcu-5.1.8";

    sha256 = "01dfbf0245d8cc0f51ba16467a60b5fad08e30b28df7846e0dd213da1143ecce";

    doCheck = true;
    checkTarget = "test";
    checkFlagsArray = ["REPORT_EXIT_STATUS=1" "NO_INTERACTION=1" "TEST_PHP_DETAILED=1"];
  };

  imagick = buildPecl {
    name = "imagick-3.4.3RC1";
    sha256 = "0siyxpszjz6s095s2g2854bhprjq49rf22v6syjiwvndg1pc9fsh";
    configureFlags = "--with-imagick=${imagemagick.dev}";
    nativeBuildInputs = [ pkgconfig ];
    buildInputs = [ pkgconfig autoreconfHook ];

# the tests seem really slow, not sure if that's normal or not...
#    doCheck = true;
#    checkTarget = "test";
#    checkFlagsArray = ["REPORT_EXIT_STATUS=1" "NO_INTERACTION=1" "TEST_PHP_DETAILED=1"];
  };

  handlebars = buildPecl {
    name = "handlebars-0.8.2RC2";

    sha256 = "dab8a3747841719efc51b8e03b98e8a76d1324b535b7c4229fa3a44c1d5eb5f4";

    preConfigure = ''
      export PKG_CONFIG_PATH="${handlebars.dev}/lib/pkgconfig:$PKG_CONFIG_PATH"
    '';
    configureFlags = [ "--with-handlebars=${handlebars}" ];
    buildInputs = [ handlebars talloc pkgconfig autoreconfHook ];

    doCheck = true;
    checkTarget = "test";
    checkFlagsArray = ["REPORT_EXIT_STATUS=1" "NO_INTERACTION=1" "TEST_PHP_DETAILED=1"];
  };

  redis = buildPecl {
    name = "redis-3.1.4";

    sha256 = "adebdfd52e8227a4da5d381d325b6eaccd29fd233bcc1b877517b9e8706ef265";
  };

  sodium = buildPecl {
    name = "libsodium-2.0.7";

    sha256 = "565e4e6e262ed8565d960f40b4baaefe930fba1dc436562c7caf6c76303325b0";

    configureFlags = [ "--with-sodium=${libsodium.dev}" ];
    buildInputs = [ libsodium pkgconfig autoreconfHook ];

    doCheck = true;
    checkTarget = "test";
# tests/pwhash_argon2i.phpt is currently failing
#    checkFlagsArray = ["REPORT_EXIT_STATUS=1" "NO_INTERACTION=1" "TEST_PHP_DETAILED=1"];
  };

  psr = buildPecl {
    name = "psr-0.3.0RC4";

    sha256 = "3628bd8f7d6ccccb53d5b655152f5b1125e1ee87e2bdd3fe7222aafd8838d91c";

    doCheck = true;
    checkTarget = "test";
    checkFlagsArray = ["REPORT_EXIT_STATUS=1" "NO_INTERACTION=1" "TEST_PHP_DETAILED=1"];
  };

  uuid = buildPecl {
    name = "uuid-1.0.4";

    sha256 = "63079b6a62a9d43691ecbcd4eb52e5e5fe17b5a3d0f8e46e3c17ff265c06a11f";

    nativeBuildInputs = [ pkgconfig ];
    buildInputs = [ libuuid ];
    configureFlags = "--with-uuid=${libuuid.dev}";

    doCheck = true;
    checkTarget = "test";
    checkFlagsArray = ["REPORT_EXIT_STATUS=1" "NO_INTERACTION=1" "TEST_PHP_DETAILED=1"];
  };

}; in self

