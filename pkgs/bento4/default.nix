{ stdenv, fetchurl, pkgconfig, glib, cmake, python27Full, makeWrapper }:

stdenv.mkDerivation rec {
  name = "bento4-1.5.1-620";
  
  src = fetchurl {
    url = https://github.com/axiomatic-systems/Bento4/archive/v1.5.1-620.tar.gz;
    sha256 = "4511f9d28a5c11d6829b0a39597e0c5aa1cab08ccb281342635eb185bfdbdd95";
  };
  patches = [ ./cmake-install.patch ];
  outputs = [ "out" ];
  
  buildInputs = [ makeWrapper pkgconfig glib python27Full ];
  nativeBuildInputs = [ cmake ];
  enableParallelBuilding = true;
  doCheck = false;

  preDistPhases = [ "bentoPreDistPhase" ];
  bentoPreDistPhase = ''
    makeWrapper "${python27Full.interpreter}" "$out"/bin/mp4-dash --add-flags $out/share/bento4/mp4-dash.py
    makeWrapper "${python27Full.interpreter}" "$out"/bin/mp4-dash-clone --add-flags $out/share/bento4/mp4-dash-clone.py
    makeWrapper "${python27Full.interpreter}" "$out"/bin/mp4-dash-encode --add-flags $out/share/bento4/mp4-dash-encode.py
    makeWrapper "${python27Full.interpreter}" "$out"/bin/mp4-hls --add-flags $out/share/bento4/mp4-hls.py
  '';

  meta = with stdenv.lib; {
    description = "Full-featured MP4 format and MPEG DASH library and tools";
    homepage = http://www.bento4.com/;
    license = "GPLv2.0";
    maintainers = [ "John Boehr <jbboehr@gmail.com>" ];
  };
}
