source $stdenv/setup

buildPhase() {
    echo do nothing
}

installPhase() {
    mkdir -p $out/share/handlebars-spec
    cp -prvd spec export $out/share/handlebars-spec/
}

genericBuild
