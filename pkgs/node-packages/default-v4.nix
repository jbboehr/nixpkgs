{pkgs, system, nodejs}:

let
  nodePackages = import ./composition-v4.nix {
    inherit pkgs system nodejs;
  };
in
nodePackages // {}
