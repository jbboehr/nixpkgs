{pkgs, system, nodejs}:

let
  nodePackages = import ./composition-v6.nix {
    inherit pkgs system nodejs;
  };
in
nodePackages // {}
