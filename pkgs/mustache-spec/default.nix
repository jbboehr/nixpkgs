{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  name = "mustache-spec-1.1.2";

  builder = ./builder.sh;

  src = fetchurl {
    url = https://github.com/mustache/spec/archive/v1.1.2.tar.gz;
    sha256 = "477552869cf4a8d3cadb74f0d297988dfa9edddbc818ee8f56bae0a097dc657c";
  };

  meta = {
    description = "The mustache spec";
    homepage = https://github.com/mustache/spec;
    maintainers = [  ];
  };
}
