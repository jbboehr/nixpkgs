source $stdenv/setup

buildPhase() {
    echo do nothing
}

installPhase() {
    mkdir -p $out/share/mustache-spec
    cp -prvd specs $out/share/mustache-spec/
}

genericBuild
