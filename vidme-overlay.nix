self: super:

let
  recurseIntoAttrs = attrs: attrs // { recurseForDerivations = true; };
  callPackage = super.pkgs.lib.callPackageWith (super.pkgs // self);
in
rec {
    bento4 = callPackage ./pkgs/bento4 { };
    cfssl = callPackage ./pkgs/cfssl { };
    goss = callPackage ./pkgs/goss { };
    mustache_spec = callPackage ./pkgs/mustache-spec { };
    handlebars_spec = callPackage ./pkgs/handlebars-spec { };
    handlebars = callPackage ./pkgs/handlebars { };

    php70Packages = super.php70Packages // recurseIntoAttrs (callPackage ./pkgs/php-packages.nix {
      php = self.pkgs.php70;
    });

    php71Packages = super.php71Packages // recurseIntoAttrs (callPackage ./pkgs/php-packages.nix {
      php = self.pkgs.php71;
    });

    phpPackages = php71Packages;

    nodePackages_6_x = super.nodePackages_6_x // callPackage ./pkgs/node-packages/default-v6.nix {
        nodejs = super.pkgs.nodejs-6_x;
    };

    nodePackages_4_x = super.nodePackages_4_x // callPackage ./pkgs/node-packages/default-v4.nix {
        nodejs = super.pkgs.nodejs-4_x;
    };
}
