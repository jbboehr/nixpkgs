{ pkgs ? import <nixpkgs> {}, system ? builtins.currentSystem }:

let
  recurseIntoAttrs = attrs: attrs // { recurseForDerivations = true; };
  callPackage = pkgs.lib.callPackageWith (pkgs // self);
  phpConfig = (pkgs.config.php or {}) // {
      mysql = false;
      ldap = false;
      zts = false;
  };

  self = rec { 
    bento4 = callPackage ./pkgs/bento4 { };
    cfssl = callPackage ./pkgs/cfssl { };
    goss = callPackage ./pkgs/goss { };
    mustache_spec = callPackage ./pkgs/mustache-spec { };
    handlebars_spec = callPackage ./pkgs/handlebars-spec { };
    handlebars = callPackage ./pkgs/handlebars { };

    php70 = pkgs.php70.override {
         config = { php = phpConfig; };
         mysql = pkgs.mysql55;
#         mysql = pkgs.mysql57;
    };

    php71 = pkgs.php71.override {
         config = { php = phpConfig; };
         mysql = pkgs.mysql55;
#         mysql = pkgs.mysql57;
    };

    php = php71;

    php70Packages = recurseIntoAttrs (callPackage ./pkgs/php-packages.nix {
      php = php70;
      libsodium = pkgs.libsodium;
    });

    php71Packages = recurseIntoAttrs (callPackage ./pkgs/php-packages.nix {
      php = php71;
      libsodium = pkgs.libsodium;
    });

    phpPackages = php71Packages;

    nodePackages_6_x = callPackage ./pkgs/node-packages/default-v6.nix {
        nodejs = pkgs.nodejs-6_x;
    };

    nodePackages_4_x = callPackage ./pkgs/node-packages/default-v4.nix {
        nodejs = pkgs.nodejs-4_x;
    };
  };
in
self
